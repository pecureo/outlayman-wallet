'use strict';

let config = require('./webpack.config.js');
let webpack = require('webpack');

config.devServer = {
  contentBase: 'app',
  host: '0.0.0.0',
  hot: true,
  https: true,
  inline: true,
  port: 443
};
config.devtool = 'eval-source-map';
config.output.publicPath = 'https://wallet.dev.outlayman.com/';
config.plugins.push(new webpack.HotModuleReplacementPlugin());

config.resolve.alias['configuration'] = __dirname + '/app/config/development.web.js';
config.resolve.alias['nfc-reader'] = __dirname + '/app/nfc/development.js';

module.exports = config;
