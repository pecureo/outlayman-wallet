# Outlayman Administration

This is a sample administration front-end for an Outlayman server

## Setup and usage

To recreate the development setup for this front-end run `npm install`
followed by `npm run dev-server`.
