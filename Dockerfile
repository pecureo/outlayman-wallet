FROM node:6.9.1
MAINTAINER Joris Kraak <me@joriskraak.nl>

ADD bower.json /tmp/bower.json
RUN cd /tmp/ && \
    npm install -g bower && \
    bower install --allow-root && \
    mkdir -p /outlayman-wallet/ && \
    mv -f /tmp/bower_components/ /outlayman-wallet/ && \
    rm -f /tmp/bower.json && \
    npm uninstall -g bower

ADD package.json /tmp/package.json
RUN cd /tmp/ && \
    npm install && \
    mv -f /tmp/node_modules/ /outlayman-wallet/ && \
    rm -f /tmp/package.json

WORKDIR /outlayman-wallet/
ADD . /outlayman-wallet/

CMD ["npm", "start"]
