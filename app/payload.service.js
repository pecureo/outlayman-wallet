'use strict';

export default ng_app_module => {
  ng_app_module.service('PayloadService', PayloadService);
};

class PayloadService {
  constructor(ErrorService) {
    this.ErrorService = ErrorService;
  }

  encrypt(payload) {
    return encodeURIComponent(btoa(JSON.stringify(payload)));
  }

  decrypt(payload) {
    try {
      let decoded = JSON.parse(atob(decodeURIComponent(payload)));
      return decoded;
    } catch(exception) {
      this.ErrorService.add('', 'Could not decode payload');
    }

    return null;
  }
}
