'use strict';

import Rx from 'rx';

export default {
  is_available$: Rx.Observable.of(false),
  reader: Rx.Observable.never()
};
