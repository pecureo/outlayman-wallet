'use strict';

import ReaderFactory from './reader-factory.js';

export default ng_app_module => {
  ReaderFactory(ng_app_module);
};
