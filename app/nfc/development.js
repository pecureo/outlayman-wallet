'use strict';

import Rx from 'rx';

export default {
  is_available$: Rx.Observable.of(true),
  reader: Rx.Observable.interval(5000).map(() => 'deadbeefc0de' + (Math.random() * 1000).toFixed(0))
};
