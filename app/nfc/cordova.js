'use strict';

import Rx from 'rx';

let is_available$ = new Rx.BehaviorSubject(false);

export default {
  is_available$,
  reader: Rx.Observable.create(function(observer) {
    // The NFC plugin is only available once the deviceready event has fired
    document.addEventListener('deviceready', nfcTagDiscoveryListener, false);

    function tagDiscovered(event) {
      observer.onNext(nfc.bytesToHexString(event.tag.id));
    }

    function registerSuccess() {
      is_available$.onNext(true);
    }

    function registerFailed() {
      is_available$.onNext(false);
    }

    function nfcTagDiscoveryListener() {
      try {
        nfc.addTagDiscoveredListener(tagDiscovered, registerSuccess, registerFailed);
      } catch(exception) {
        console.error('NFC reader not available', exception);
      }
    }
  })
};
