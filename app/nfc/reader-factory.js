'use strict';

import nfc_reader from 'nfc-reader';

export default ng_app_module => {
  ng_app_module.factory('NfcReader', NfcReaderFactory);
};

function NfcReaderFactory() {
  // The returned observable has to be turned into a hot observable to make
  // sure only newly arrived NFC identifiers are processed by observers
  nfc_reader.reader = nfc_reader.reader.publish();
  nfc_reader.reader.connect();

  return nfc_reader;
}
