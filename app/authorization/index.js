'use strict';

import Rx from 'rx';

import './index.scss';

// The following are dependencies of Anvil, these are installed through bower
// and need some special care to be loaded properly. A lot of their
// functionality is expected to be on the window object. The following code
// tries to minimize those occurrences
window.sjcl = require('sjcl');

window.CryptoJS = require('exports?CryptoJS!crypto-js/core');
require('crypto-js/sha256');

window.b64tohex = require('exports?b64tohex!jsrsasign/ext/base64');

let jsbn = require('exports?BigInteger&int2char!jsrsasign/ext/jsbn');
window.BigInteger = jsbn.BigInteger;
window.int2char = jsbn.int2char;

let rsa = require('exports?RSAKey&parseBigInt!jsrsasign/ext/rsa');
window.RSAKey = rsa.RSAKey;
window.parseBigInt = rsa.parseBigInt;
window._rsasign_verifySignatureWithArgs = require('exports?_rsasign_verifySignatureWithArgs!jsrsasign/rsasign-1.2');

window.ASN1HEX = require('exports?ASN1HEX!jsrsasign/asn1hex-1.1');
window.KJUR = require('imports?KJUR=>{}!exports?KJUR!jsrsasign/crypto-1.1');

let base64x = require('exports?b64utohex&b64utoutf8!jsrsasign/base64x-1.1');
window.b64utohex = base64x.b64utohex;
window.b64utoutf8 = base64x.b64utoutf8;

window.jsonParse = require('exports?jsonParse!jsjws/ext/json-sans-eval');
require('jsjws/jws-2.0');

import 'anvil-connect/anvil-connect.angular.js';

export default ng_app_module => {
  ng_app_module.provider('Authorization', AuthorizationProvider);

  ng_app_module.component('authorizer', {
    bindings: {
      authorizedTarget: '<', // the uri to redirect to after authorization
      expired: '<',
      signedOut: '<'
    },
    controller: AuthorizerController,
    template: require('./authorizer.html')
  });

  ng_app_module.run(function(Anvil) {
    Anvil.getKeys();
  });
};

function AuthorizationProvider($routeProvider, AnvilProvider, configuration) {
  $routeProvider.when('/authorize', {
    resolve: {
      authorized_target: ($route, configuration) => {
        return $route.current.params.after_authorization_target ||
               configuration.default_authorized_target;
      },
      expired: $route => ($route.current.params.expired === 'true'),
      signed_out: $route => ($route.current.params.signed_out === 'true')
    },
    template: `
      <authorizer authorized_target="$resolve.authorized_target"
                  expired="$resolve.expired" flex signed_out="$resolve.signed_out">
      </authorizer>
    `
  });

  AnvilProvider.configure(configuration.anvil);

  return {
    $get($location, $timeout, $window, Anvil) {
      let reauthorization_timeout;
      let session$ = new Rx.BehaviorSubject(Anvil.session);
      let scopes$ = new Rx.BehaviorSubject(new Scopes());

      // Authorization data may still be available from a prior session. Any
      // subscribers should be notified of this existing session.
      notifySessionSubscribers();

      function authorize(
        destination_uri = configuration.default_authorized_target,
        requested_scopes = configuration.required_scopes,
        max_age = 3600
      ) {
        AnvilProvider.configure(Object.assign(
          { scope: requested_scopes }, configuration.anvil
        ));

        Anvil.destination(destination_uri);

        // The lower-level $window.location is used to force a full page reload
        $window.location.href = `${Anvil.uri('connect/google')}&max_age=${max_age}`;
      }

      function getHeaders(headers = {}) {
        return Anvil.headers(headers);
      }

      function notifySessionSubscribers() {
        session$.onNext(Anvil.session);

        // A separate stream is available to just listen for authorized scopes
        if(Anvil.session.access_claims && Anvil.session.access_claims.scope) {
          scopes$.onNext(new Scopes(Anvil.session.access_claims.scope));
        } else {
          scopes$.onNext(new Scopes());
        }
      }

      // This is mostly Anvil.callback, however, since that function has
      // verification and parsing of the authorization data mixed with actual
      // replacement of the session it cannot be used for all Outlayman
      // Wallet's authorization needs. Therefore its functionality has been
      // replicated here with minor adjustments split across two functions
      // (process and updateSession)
      function process(authorization_data) {
        authorization_data = Anvil.parseFormUrlEncoded(authorization_data);

        if(authorization_data.error) {
          return authorization_data.error;
        } else {
          let accessJWS = new window.KJUR.jws.JWS();
          let idJWS = new window.KJUR.jws.JWS();

          // Get the JWKs from localStorage. This should be readily available
          // if the user is signed in
          let jwk = JSON.parse(localStorage['anvil.connect.jwk']);
          let hN = window.b64utohex(jwk.n);
          let hE = window.b64utohex(jwk.e);

          // Decode the access token and verify signature
          if(authorization_data.access_token &&
             !accessJWS.verifyJWSByNE(authorization_data.access_token, hN, hE)) {
            return { error: 'Failed to verify access token signature' };
          }

          // Decode the id token and verify signature
          if(authorization_data.id_token &&
             !idJWS.verifyJWSByNE(authorization_data.id_token, hN, hE)) {
            return { error: 'Failed to verify id token signature' };
          }

          // Parse the access token payload
          try {
            authorization_data.access_claims = JSON.parse(
              accessJWS.parsedJWS.payloadS
            );
          } catch(e) {
            return { error: 'Cannot parse access token payload' };
          }

          // Parse the id token payload
          try {
            authorization_data.id_claims = JSON.parse(idJWS.parsedJWS.payloadS);
          } catch(e) {
            return { error: 'Cannot parse id token payload' };
          }

          // Validate the nonce
          if(authorization_data.id_claims &&
             !Anvil.nonce(authorization_data.id_claims.nonce)) {
            return { error: 'Invalid nonce' };
          }

          // Verify at_hash
          let atHash = window.CryptoJS.SHA256(authorization_data.access_token)
                               .toString(window.CryptoJS.enc.Hex);
          atHash = atHash.slice(0, atHash.length / 2);
          if(authorization_data.id_claims &&
             atHash !== authorization_data.id_claims.at_hash) {
            return { error: 'Invalid access token hash in id token payload' };
          }

          return authorization_data;
        }
      }

      function signout(redirect_to = '/') {
        Anvil.reset();
        notifySessionSubscribers();

        if(reauthorization_timeout) {
          $timeout.cancel(reauthorization_timeout);
        }

        $location.url(redirect_to);
      }

      function updateSession(session) {
        Anvil.session = session;

        Anvil.userInfo().then(user_info => {
          Anvil.session.user_info = user_info;
          Anvil.serialize();
          notifySessionSubscribers();
        }).catch(error => session$.onError(error));

        // Token expirations are in seconds
        let expires_in_s = (Anvil.session.id_claims.exp * 1000) - Date.now();
        reauthorization_timeout = $timeout(expires_in_s).then(() => {
          signout(`/authorize?expired=true&after_authorization_target=${$location.url()}`);
        });
      }

      return {
        authorize,
        destination: Anvil.destination,
        getHeaders,
        implicit: !configuration.show_authorizer,
        process,
        scopes$,
        session$,
        signout,
        updateSession
      };
    }
  };
}

class AuthorizerController {
  constructor($analytics, $location, Authorization) {
    this.$analytics = $analytics;
    this.Authorization = Authorization;

    Authorization.session$.subscribe(session => {
      if(session.access_claims) {
        $location.path(this.authorizedTarget).replace();
      } else if(Authorization.implicit && !this.expired) {
        // Only enforce implicit sign in if the authorization request is not a
        // result of expired authorizations to prevent unexpected page reloads
        this.signin();
      }
    });
  }

  signin() {
    this.$analytics.eventTrack('sign_in:initialize', { category: 'credential' });
    this.signing_in = true;
    this.Authorization.authorize(this.authorizedTarget);
  }
}

class Scopes {
  constructor(scopes_string) {
    if(scopes_string && scopes_string.length > 0) {
      this.scopes = scopes_string.split(' ');
    } else {
      this.scopes = [];
    }

    this.length = this.scopes.length;
  }

  includes(...requested_scopes) {
    return requested_scopes.every(requested_scope => {
      return this.scopes.indexOf(requested_scope) >= 0;
    });
  }
}
