'use strict';

let crypto = require('crypto');

export function browserGoogleAnalyticsTrackerCode(tracking_id) {
  let tracker_code = `(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '${tracking_id}', 'auto');`;

  return {
    nonce: crypto.createHash('md5').update(tracker_code).digest('hex'),
    tracker_code
  };
}

export function cordovaGoogleAnalyticsTrackerCode(tracking_id) {
  // For the tracker code to work on mobile devices it needs to always load the
  // script over https, as well as use the device's UUID as the clientId. For
  // the device's UUID to be available, the tracker code cannot be run until
  // the `deviceready` event fires.
  let tracker_code = `document.addEventListener('deviceready', function() {
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', '${tracking_id}', { 'storage': 'none', 'clientId': device.uuid });
    ga('set', 'checkProtocolTask', null);
  }, false);`;

  return {
    nonce: crypto.createHash('md5').update(tracker_code).digest('hex'),
    tracker_code
  };
}

export const configuration = {
  analytics: {
    nonce: 'tracker_code_nonde',
    tracker_code: 'google_analytics_tracker_code'
  },
  anvil: {
    issuer: 'issuer.uri.of.anvil.instance',
    client_id: 'client_id',
    redirect_uri: 'registered_redirect_uri_of_application'
  },
  // Should contain a trailing slash
  base_href: '/',
  csp: {},
  default_authorized_target: '/profile',
  required_scopes: [
    'announcement:read', 'credential:manage_own', 'credential:view_own',
    'email', 'identifier_link_code:create', 'identifier_link_code:read',
    'transaction:benefactor', 'wallet:manage_own', 'wallet:view_own'
  ],
  show_authorizer: true,
  uri: {
    // These should _NOT_ contain a trailing slash
    api: 'uri.of.api',
    website: 'uri.of.website'
  },
  version: require('json!../../package.json').version
};
