'use strict';

export default ng_app_module => {
  ng_app_module.service('ErrorService', ErrorService);
};

class ErrorService {
  constructor() {
    this.errors = [];
    this.next_id = 1;
  }

  add(header, message) {
    this.errors.push({ id: this.next_id++, header, message });
  }

  list() {
    return this.errors;
  }

  remove(id) {
    this.errors = this.errors.filter(error => error.id !== id);
  }
}
