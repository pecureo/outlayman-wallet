'use strict';

import Rx from 'rx';

export default ng_app_module => {
  ng_app_module.directive('editableListItem', EditableListItemDirective);
};

function EditableListItemDirective() {
  return {
    bindToController: {
      placeholder: '=',
      onRemove: '&',
      onSave: '&',
      removable: '=',
      value: '='
    },
    controller: EditableListItemController,
    controllerAs: 'editableListItem',
    link: editableListItemLink,
    restrict: 'E',
    scope: true,
    template: require('./editable-list-item.html')
  };
}

class EditableListItemController {
}

function editableListItemLink(scope, element, attributes, controller) {
  Rx.Observable.fromEvent(element, 'input')
    .debounce(250)
    .pluck('target', 'value')
    .distinctUntilChanged()
    .subscribe(new_value => {
      controller.value = new_value;
      controller.onSave({ value: new_value });
    });
}
