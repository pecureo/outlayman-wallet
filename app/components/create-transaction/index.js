'use strict';

import qr from 'qr-image';

export default ng_app_module => {
  ng_app_module.component('createTransaction', {
    bindings: { payload: '<' },
    controller: CreateTransactionComponent,
    template: require('./create-transaction.html')
  });
};

class CreateTransactionComponent {
  constructor(Authorization, CredentialService, ErrorService, PayloadService,
    WalletService, $location, $scope, $timeout, configuration, restmod) {

    this.ErrorService = ErrorService;
    this.PayloadService = PayloadService;
    this.$location = $location;
    this.configuration = configuration;

    this.TransactionModel = restmod.model(`${configuration.uri.api}/transaction`).mix({
      $hooks: {
        'before-request': request => {
          request.headers = Authorization.getHeaders(request.headers);
        }
      }
    });

    WalletService.wallets$.subscribe(wallets => {
      this.wallets = wallets;
      $timeout($scope.$apply.bind($scope));
    });

    Authorization.scopes$.subscribe(scopes => {
      if(scopes.includes('transaction:benefactor')) {
        this.reset();

        if(this.payload) {
          // The beneficiary from a transaction request needs to be stored
          // separately so it can be made available from the beneficiary
          // selection
          this.transaction = PayloadService.decrypt(this.payload);
          this.transaction.request_beneficiary = this.transaction.beneficiary;
        }
      }
    });
  }

  benefactorWallets() {
    if(this.transaction) {
      return this.wallets.filter(wallet => {
        return (!this.transaction.beneficiary || (this.transaction.beneficiary.id !== wallet.id)) &&
               (!this.transaction.request_beneficiary || (this.transaction.request_beneficiary.id !== wallet.id));
      });
    } else {
      return this.wallets;
    }
  }

  beneficiaryWallets() {
    let beneficiary_wallets = this.wallets.filter(wallet => this.transaction.benefactor !== wallet);

    if(this.transaction && this.transaction.request_beneficiary) {
      return [ this.transaction.request_beneficiary ].concat(beneficiary_wallets.filter((wallet) => {
        return (this.transaction.request_beneficiary.id !== wallet.id);
      }));
    } else {
      return beneficiary_wallets;
    }
  }

  canCreateTransaction() {
    return this.transaction && this.transaction.amount &&
      this.transaction.benefactor && this.transaction.beneficiary;
  }

  canCreateTransactionRequest() {
    return this.transaction && this.transaction.amount &&
      this.transaction.beneficiary && !this.transaction.benefactor;
  }

  canReset() {
    return this.transaction &&
      (this.transaction.amount || this.transaction.benefactor ||
       this.transaction.beneficiary || this.transaction.description);
  }

  clearWallet(wallet) {
    delete this.transaction[wallet];
  }

  createTransaction() {
    let transaction_payload = {
      amount: this.transaction.amount,
      benefactor: this.transaction.benefactor.id,
      beneficiary: this.transaction.beneficiary.id
    };

    if(this.transaction.description) {
      transaction_payload.description = {
        benefactor: this.transaction.benefactor.description,
        beneficiary: this.transaction.beneficiary.description,
        comment: this.transaction.description
      };
    }

    this.TransactionModel.$create(transaction_payload).$then(() => {
      this.wallets.forEach(wallet => {
        if(wallet.id == transaction_payload.benefactor || wallet.id == transaction_payload.beneficiary) {
          wallet.refresh();
        }
      });

      this.$location.path('profile');
    }, error => {
      let reason = '';

      if(error.$response.data.code === 'E_VALIDATION') {
        reason = 'One or more fields contained invalid values';
      }

      this.ErrorService.add('Could not create the transaction', reason);
    });
  }

  createTransactionRequest() {
    this.transaction.request = {
      amount: this.transaction.amount,
      beneficiary: {
        id: this.transaction.beneficiary.id,
        description: `${this.transaction.beneficiary.description} (${this.transaction.beneficiary.owner.uuid})`
      },
      description: this.transaction.description
    };
    this.transaction.request.payload = this.PayloadService.encrypt(this.transaction.request);

    let uri_parameters = `#/create-transaction?payload=${this.transaction.request.payload}`;
    this.transaction.request.uri = `${this.configuration.uri.website}/${uri_parameters}`;

    let qr_code = qr.svgObject(`outlaymanwallet://${uri_parameters}`);
    this.transaction.request.qr = {
      path: qr_code.path,
      view_box: `0 0 ${qr_code.size} ${qr_code.size}`
    };
  }

  reset() {
    this.transaction = {};
  }
}
