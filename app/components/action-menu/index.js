'use strict';

import Rx from 'rx';

export default ng_app_module => {
  ng_app_module.component('actionMenu', {
    bindings: {
      header: '<',
      side: '@',
      title: '@'
    },
    controller: ActionMenuController,
    template: require('./action-menu.html')
  });

  ng_app_module.service('ActionMenuService', ActionMenuService);
};

class ActionMenuController {
  constructor($location, $mdSidenav, $scope, $timeout, ActionMenuService) {
    this.$location = $location;
    this.$scope = $scope;
    this.$timeout = $timeout;

    if(!this.side) {
      this.side = 'left';
    }

    if(!this.title) {
      this.title = `Action Menu ${ActionMenuService.menu_count + 1}`;
    }

    this.id = this.title.replace(' ', '-').toLowerCase();

    this.actions = {
      primary: [],
      secondary: []
    };

    $mdSidenav(this.id, true).then(sidenav => this.sidenav = sidenav);

    ActionMenuService.register(this);
  }

  close() {
    return this.sidenav.close();
  }

  execute(action, event) {
    if(typeof(action.payload) === 'string') {
      // Assume string actions to be navigation events within the application
      this.$location.path(action.payload);
    } else {
      action.payload(event);
    }

    this.close();
  }

  isOpen() {
    return this.sidenav.isOpen();
  }

  isRegistered(action, type = 'primary') {
    return this.actions[type].some(registered_action => {
      return (registered_action == action);
    });
  }

  open() {
    return this.sidenav.open();
  }

  register(action, type = 'primary') {
    if(!this.isRegistered(action, type)) {
      this.actions[type].push(action);

      this.actions[type].sort((action1, action2) => {
        let weight1 = action1.weight || 0;
        let weight2 = action2.weight || 0;

        // Using weights the order of action menu items can be influenced
        if(weight1 < weight2) {
          return -1;
        } else if(weight1 > weight2) {
          return 1;
        } else {
          // In case of equal weights the order is determined by the
          // description of the item
          if(action1.description < action2.description) {
            return -1;
          } else {
            return 1;
          }
        }
      });

      this.$timeout(this.$scope.$apply.bind(this.$scope));
    }
  }

  toggle() {
    return this.sidenav.toggle();
  }

  unregister(action, type = 'primary') {
    this.actions[type] = this.actions[type].filter(stored_action => {
      return (action.payload !== stored_action.payload);
    });
    this.$timeout(this.$scope.$apply.bind(this.$scope));
  }
}

class ActionMenuService {
  constructor() {
    this.menus = {};
  }

  getMenu$(id) {
    if(!this.menus[id]) {
      this.menus[id] = new Rx.Subject();
    }

    return this.menus[id];
  }

  get menu_count() {
    return Object.keys(this.menus).length;
  }

  register(action_menu) {
    if(!action_menu.id) {
      throw(new Error('Action Menu is Missing ID'));
    }

    if(!this.menus[action_menu.id]) {
      this.menus[action_menu.id] = new Rx.Subject();
    }

    this.menus[action_menu.id].onNext(action_menu);
  }
}
