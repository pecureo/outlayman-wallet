'use strict';

export default ng_app_module => {
  ng_app_module.component('registration', {
    controller: RegistrationController,
    template: require('./registration.html')
  });
};

class RegistrationController {
  constructor($analytics, $location, Authorization, CredentialService,
    ErrorService, configuration) {

    this.$analytics = $analytics;
    this.$location = $location;
    this.Authorization = Authorization;
    this.CredentialService = CredentialService;
    this.ErrorService = ErrorService;

    this.performing_registration = false;

    Authorization.scopes$.subscribe(scopes => {
      if(scopes.includes('credential:view_own')) {
        // At least some of the scopes necessary for the application to
        // function are available, so the registration screen should no longer
        // show up
        $location.url(configuration.default_authorized_target);
      }
    });
  }

  register() {
    this.$analytics.eventTrack('register:initialize', { category: 'credential' });
    this.performing_registration = true;

    this.CredentialService.register().then(() => {
      this.$analytics.eventTrack('register:success', { category: 'credential' });

      // Since a freshly created account will not yet have the appropriate
      // scopes the application will have to reauthorize
      this.$analytics.eventTrack('sign_in:initialize', { category: 'credential' });
      this.Authorization.authorize();
    }).catch(error => {
      this.$analytics.eventTrack('register:failure', { category: 'credential' });
      this.ErrorService.add('', error.data.error_description);
      this.performing_registration = false;
    });
  }

  cancel() {
    this.Authorization.signout('/authorize');
  }
}
