'use strict';

export default ng_app_module => {
  ng_app_module.service('IdentifierLinkCodeService', IdentifierLinkCodeService);

  ng_app_module.component('linkIdentifier', {
    bindings: { payload: '<' },
    controller: LinkIdentifierComponent,
    template: require('./link-identifier.html')
  });
};

class IdentifierLinkCodeService {
  constructor($http, Authorization, configuration) {
    this.$http = $http;

    this.Authorization = Authorization;
    this.configuration = configuration;
  }

  decrypt(encrypted_link_code, password) {
    return new Promise((resolve, reject) => {
      if(password.length > 0) {
        try {
          let link_code = window.CryptoJS.AES.decrypt(
            encrypted_link_code, password
          ).toString(window.CryptoJS.enc.Utf8);

          resolve(JSON.parse(link_code));
        } catch(exception) {
          // As long as an invalid password is provided the decryption process
          // will return invalid UTF-8 encoded bytes, so the string conversion
          // process will fail
          reject({
            type: 'invalid_password',
            message: 'Could not decrypt identifier link code'
          });
        }
      } else {
        reject({
          type: 'required',
          message: 'Passwords need to be longer than 0 characters'
        });
      }
    });
  }

  getCodeById(id) {
    return this.$http({
      headers: this.Authorization.getHeaders(),
      method: 'GET',
      url: `${this.configuration.uri.api}/identifier_link_code/${id}`
    });
  }
}

class LinkIdentifierComponent {
  constructor($analytics, $http, $scope, $timeout, Authorization, ErrorService,
    IdentifierLinkCodeService, NfcReader, PayloadService) {

    this.$analytics = $analytics;
    this.$http = $http;
    this.$timeout = $timeout;
    this.ErrorService = ErrorService;
    this.IdentifierLinkCodeService = IdentifierLinkCodeService;

    Authorization.scopes$.subscribe(scopes => {
      if(scopes.includes('identifier_link_code:read')) {
        this.step = 'link_code';

        if(this.payload) {
          this.link_code_data = PayloadService.decrypt(this.payload);

          // Need to postpone processing of the available link code data, to make
          // sure the form is available on the controller until after the
          // controller is fully initialized
          $timeout(this.getLinkCode.bind(this));
        }

        this.nfc_subscription = NfcReader.reader.subscribe(tag => {
          this.identifier = tag;

          // Postpone to be sure to not interfere with running digest cycles
          $timeout($scope.$apply.bind($scope));
        });
      }
    });
  }

  $onDestroy() {
    if(this.nfc_subscription) {
      this.nfc_subscription.dispose();
    }
  }

  addIdentifier(identifier, link_code_data) {
    this.$analytics.eventTrack('add_through_link:initialize', { category: 'identifier' });

    let identifier_data = {
      description: link_code_data.description,
      uuid: identifier
    };

    this.adding_identifier = true;

    this.$http({
      data: { credential: identifier_data },
      headers: { authorization: link_code_data.token },
      method: 'POST',
      url: link_code_data.uri
    }).then(() => {
      this.$analytics.eventTrack('add_through_link:success', { category: 'identifier' });
      this.adding_identifier = false;
      this.step = 'success';
    }).catch(() => {
      this.$analytics.eventTrack('add_through_link:failure', { category: 'identifier' });
      this.adding_identifier = false;
      this.step = 'failed';
    });
  }

  getLinkCode() {
    let { id, password } = this.link_code_data;

    return this.IdentifierLinkCodeService.getCodeById(id).then(response => {
      this.link_code_data.identifier_link_code = response.data.identifier_link_code;

      // Update error status of the input field
      this.linkCodeForm.linkCodeId.$setValidity('connection', true);
      this.linkCodeForm.linkCodeId.$setValidity('not_found', true);
      this.linkCodeForm.linkCodeId.$setTouched();

      if(password) {
        return this.IdentifierLinkCodeService.decrypt(
          this.link_code_data.identifier_link_code, password
        ).then(decrypted_link_code => {
          this.link_code_data.decrypted = decrypted_link_code;

          this.expiration_timeout = this.$timeout(() => {
            this.$analytics.eventTrack('add_through_link:expired', { category: 'identifier' });
            this.ErrorService.add('', 'The identifier link code has expired');
            this.reset();
          }, (decrypted_link_code.expires_at * 1000) - Date.now());

          this.linkCodeForm.password.$setValidity('invalid', true);
          this.linkCodeForm.password.$setValidity('required', true);
          this.linkCodeForm.password.$setTouched();
        }, error => {
          this.link_code_data.decrypted = {};

          if(error.type === 'invalid_password') {
            this.linkCodeForm.password.$setValidity('required', true);
            this.linkCodeForm.password.$setValidity('invalid', false);
          } else {
            this.linkCodeForm.password.$setValidity('required', false);
          }
          this.linkCodeForm.password.$setTouched();
        });
      }
    }).catch(error => {
      // Clear any possible retrieved or decrypted link code data in case of
      // invalid passwords to prevent sensitive data leaks
      delete this.link_code_data.identifier_link_code;
      delete this.link_code_data.decrypted;

      if(error.status === 404) {
        this.linkCodeForm.linkCodeId.$setValidity('not_found', false);
      } else {
        this.linkCodeForm.linkCodeId.$setValidity('connection', false);
      }
      this.linkCodeForm.linkCodeId.$setTouched();
    });
  }

  reset() {
    this.adding_identifier = false;
    this.identifier = null;
    this.link_code_data = {};
    this.step = 'link_code';

    if(this.linkCodeForm) {
      this.linkCodeForm.linkCodeId.$validate();
      this.linkCodeForm.password.$validate();
      this.linkCodeForm.linkCodeId.$setTouched();
      this.linkCodeForm.password.$setTouched();
    }
  }

  transitionFromLinkCodeToAddIdentifier() {
    this.identifier = null;
    this.step = 'add_identifier';
  }

  transitionFromAddIdentifierToLinkCode() {
    this.reset();
  }

  transitionFromFailedToAddIdentifier() {
    this.step = 'add_identifier';
  }
}
