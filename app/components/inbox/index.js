'use strict';

import Rx from 'rx';

export default ng_app_module => {
  ng_app_module.component('inbox', {
    controller: InboxController,
    template: require('./inbox.html')
  });
  ng_app_module.service('AnnouncementService', AnnouncementService);
};

class InboxController {
  constructor($analytics, $mdDialog, $window, AnnouncementService,
    Authorization) {

    this.$analytics = $analytics;
    this.$mdDialog = $mdDialog;
    this.$window = $window;
    this.AnnouncementService = AnnouncementService;

    Authorization.scopes$.subscribe(scopes => {
      if(scopes.includes('announcement:read')) {
        AnnouncementService.announcement$.subscribe(announcements => {
          this.messages = announcements;
        });
      }
    });
  }

  confirmMarkAllAsRead(event) {
    let confirmation_dialog = this.$mdDialog.confirm()
      .cancel('No')
      .clickOutsideToClose(true)
      .ok('Yes')
      .targetEvent(event)
      .textContent('Are you sure you want to mark all messages as read?');

    this.$mdDialog.show(confirmation_dialog).then(() => {
      this.AnnouncementService.markAllAsRead();
      this.AnnouncementService.refresh();
    }, () => {
      this.$analytics.eventTrack('hide_all:abort', { category: 'inbox' });
    });
  }

  confirmMarkAsRead(event, message) {
    let confirmation_dialog = this.$mdDialog.confirm()
      .cancel('No')
      .clickOutsideToClose(true)
      .ok('Yes')
      .targetEvent(event)
      .textContent(`Are you sure you want to mark '${message.title}' as read?`);

    this.$mdDialog.show(confirmation_dialog).then(() => {
      this.AnnouncementService.markAsRead(message);
      this.AnnouncementService.refresh();
    }, () => {
      this.$analytics.eventTrack('hide:abort', { category: 'inbox' });
    });
  }

  navigateTo(url) {
    if(url) {
      this.$window.open(url, '_system');
    }
  }
}

class AnnouncementService {
  constructor($analytics, $http, Authorization, configuration) {
    this.$analytics = $analytics;
    this.$http = $http;
    this.Authorization = Authorization;
    this.configuration = configuration;

    this.announcement$ = new Rx.BehaviorSubject([]);
    this.marked_as_read_key = 'outlayman.announcements.marked_as_read';

    Authorization.scopes$.subscribe(scopes => {
      if(scopes.includes('announcement:read')) {
        this.refresh();
      }
    });
  }

  getMarkedAsRead() {
    return JSON.parse(localStorage[this.marked_as_read_key] || '[]');
  }

  markAllAsRead() {
    localStorage['outlayman.announcements.hide_before'] = Date.now();
    this.$analytics.eventTrack('hide_all:success', { category: 'inbox' });
  }

  markAsRead(announcement) {
    let marked_announcements = this.getMarkedAsRead();
    marked_announcements.push(announcement.id);

    // Make sure to only store marked announcement IDs once
    marked_announcements = marked_announcements.reduce((announcements, announcement) => {
      if(announcements.indexOf(announcement) === -1) {
        announcements.push(announcement);
      }

      return announcements;
    }, []);

    localStorage[this.marked_as_read_key] = JSON.stringify(marked_announcements);

    this.$analytics.eventTrack('hide:success', { category: 'inbox' });
  }

  refresh() {
    let current_time = new Date().toISOString();
    let hide_before = new Date(
      parseInt(localStorage['outlayman.announcements.hide_before'], 10) || 0
    );

    let announcement_filter = {
      where: {
        createdAt: { '>': hide_before.toISOString(), '<=': current_time },
        or: [
          { expires: null },
          { expires: { '>': current_time } }
        ]
      }
    };

    let marked_as_read = this.getMarkedAsRead();
    if(marked_as_read.length > 0) {
      announcement_filter.where.id = { '!': marked_as_read };
    }

    this.$http({
      headers: this.Authorization.getHeaders(),
      method: 'GET',
      url: `${this.configuration.uri.api}/announcement`,
      params: announcement_filter
    }).then(response => this.announcement$.onNext(response.data));
  }
}
