'use strict';

export default ng_app_module => {
  ng_app_module.component('profile', {
    controller: ProfileController,
    template: require('./profile.html')
  });
};

class ProfileController {
  constructor($location, $scope, $timeout, Authorization, WalletService) {

    Authorization.scopes$.subscribe(scopes => {
      if(scopes.includes('credential:view_own', 'wallet:view_own')) {
        // The provided scopes allow viewing owned credentials and wallets, so
        // wait for them to become available to be presented on the page
        WalletService.wallets$.subscribe(wallets => {
          if(wallets.length === 1) {
            // The profile view should only be used when multiple wallets are
            // available as it mostly serves as an overview
            $location.path(`/wallet/${wallets[0].id}`).replace();
          } else {
            this.wallets = wallets;
          }
          $timeout($scope.$apply.bind($scope));
        });
      }
    });
  }
}
