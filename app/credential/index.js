'use strict';

import _ from 'lodash';
import Rx from 'rx';

export default function(ng_app_module) {
  ng_app_module.service('CredentialService', CredentialService);

  ng_app_module.factory('RestmodCredential', function(configuration, restmod) {
    return restmod.model(`${configuration.uri.api}/credential`).mix({
      wallets: { belongsToMany: 'RestmodWallet', keys: 'wallets' }
    });
  });
}

class CredentialService {
  constructor($http, Authorization, ErrorService, RestmodCredential,
    configuration) {

    this.$http = $http;
    this.Authorization = Authorization;
    this.ErrorService = ErrorService;
    this.RestmodCredential = RestmodCredential;
    this.configuration = configuration;

    this.credentials = {};
    this.credentials$ = new Rx.BehaviorSubject([]);

    Authorization.session$.subscribe(session => {
      if(session.user_info) {
        this.user_info = session.user_info;

        RestmodCredential.$find(session.user_info.sub).$then(credential_data => {
          this.credentials[credential_data.uuid] = credential_data;
          this._notifySubscribers();
        }, () => {
          // If a user has not completed registration yet, access will fail due
          // to insufficient scopes. The only way to verify this, is by
          // actually checking the scopes which are granted upon registration
          if(!session.access_claims || session.access_claims.scope.indexOf('wallet:view_own') >= 0) {
            ErrorService.add('',
              `Unknown UUID ${session.user_info.sub} specified or access was denied`);
          }
        });
      } else {
        // In case no user information is available on the session, either it
        // never was or any information that was cached by the service based on
        // a previous session should be cleared
        this.credentials = {};
        this._notifySubscribers();
      }
    });
  }

  register() {
    let description = 'Unspecified';
    if(this.user_info) {
      // TODO: Sending this to the API will fail, as it is not serialized as a
      // string. Eventually this is probably the information that should be
      // stored. However, at this time all applications expect the description
      // to just be a string (for instance when showing the name of the
      // account)
      description = { email: this.user_info.email, name: this.user_info.name };
      description = this.user_info.name;
    }

    // The UUID is determined server-side from the authorization headers, so
    // doesn't appear here!
    return this.$http({
      data: { description },
      headers: this.Authorization.getHeaders(),
      method: 'POST',
      url: `${this.configuration.uri.api}/credential/register`
    });
  }

  update(uuid, field, value) {
    let credential = this.credentials[uuid];

    let update_field = credential => {
      credential[field] = value;
      credential.$save([field]).$then(
        () => this._notifySubscribers(),
        () => this.ErrorService.add('', `Could not update ${field} of ${uuid}`)
      );
    };

    if(credential) {
      update_field(credential);
    } else {
      this.RestmodCredential.$find(uuid).$then(
        credential => update_field(credential),
        () => this.ErrorService.add('', `Unknown credential ${uuid} to update`)
      );
    }
  }

  _notifySubscribers() {
    this.credentials$.onNext(_.values(this.credentials));
  }
}
