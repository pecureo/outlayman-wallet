'use strict';

import _ from 'lodash';
import Rx from 'rx';
import qr from 'qr-image';

import Component from './component';

export default function(ng_app_module) {
  Component(ng_app_module);

  ng_app_module.factory('RestmodWallet', function(configuration, restmod) {
    return restmod.model(`${configuration.uri.api}/wallet`).mix({
      credentials: { belongsToMany: 'RestmodCredential', keys: 'credentials' }
    });
  });

  ng_app_module.service('WalletService', WalletService);
}

class Wallet {
  constructor(_model) {
    this._model = _model;

    this.populateWallet(this._model);
  }

  populateWallet(wallet_model) {
    this._model = wallet_model;

    this.balance = parseFloat(this._model.balance, 10);
    this.credentials = this._model.credentials;
    this.description = this._model.description;
    this.id = this._model.id;
    this.owner = this._model.owner;

    let nfc_credentials = this._model.credentials.filter(
      ({ uuid }) => (uuid !== this.owner.uuid)
    );
    this.has_nfc_credentials = (nfc_credentials.length > 0);

    this.transactions = this.summarizeTransactions(this._model);
  }

  refresh() {
    return this._model.$refresh().$then(this.populateWallet.bind(this)).$asPromise();
  }

  summarizeTransactions(wallet_data) {
    let benefactor_transactions = _.map(wallet_data.benefactorTransactions, transaction => {
      transaction.amount = -transaction.amount;

      if(transaction.description && transaction.description.comment) {
        transaction.description = `${transaction.description.comment} to ${transaction.description.beneficiary}`;
      }

      return transaction;
    });

    let beneficiary_transactions = _.map(wallet_data.beneficiaryTransactions, transaction => {
      if(transaction.description && transaction.description.comment) {
        transaction.description = `${transaction.description.comment} from ${transaction.description.benefactor}`;
      }

      return transaction;
    });

    return _.sortBy(
      _.flatten(benefactor_transactions.concat(beneficiary_transactions)),
      'createdAt'
    ).reverse();
  }
}

class WalletService {
  constructor(Authorization, CredentialService, ErrorService, PayloadService,
    $analytics, $location, $http, configuration) {

    this.Authorization = Authorization;
    this.CredentialService = CredentialService;
    this.ErrorService = ErrorService;
    this.PayloadService = PayloadService;
    this.$analytics = $analytics;
    this.$http = $http;
    this.$location = $location;
    this.configuration = configuration;

    this.link_code_data_key = 'outlayman.link_code_data';
    this.wallets = {};
    this.wallets$ = new Rx.BehaviorSubject([]);

    CredentialService.credentials$.subscribe(credentials => {
      // First flatten the wallets for each credential
      let wallets = credentials.reduce((wallets, credential) => {
        return wallets.concat(credential.wallets);
      }, []);

      Object.keys(this.wallets).forEach(wallet_id => {
        if(!wallets.some(wallet => wallet.id === wallet_id)) {
          delete this.wallets[wallet_id];
        }
      });

      // Then instantiate proper wallet representations for any non-existent ones
      let wallet_promises = [];
      wallets.forEach(wallet => {
        if(!this.wallets[wallet.id]) {
          wallet_promises.push(wallet.$fetch().$then(wallet => {
            this.wallets[wallet.id] = new Wallet(wallet);
          }).$asPromise());
        }
      });

      // Only notify subscribers once all wallets have been processed, this
      // allows downstream checks on the number of wallets. This is not
      // possible if a notification occurs for every wallet that is added to
      // the collection. Furthermore notifying more often creates unnecessary
      // overhead, as wallets are retrieved in batches anyway.
      Promise.all(wallet_promises).then(() => {
        this._notifySubscribers();
      });
    });
  }

  addCredentialsToLinkCode(credentials) {
    let link_code_data = this.getLinkCodeData();

    link_code_data.token = credentials.access_token;
    link_code_data.claims = credentials.access_claims;

    localStorage[this.link_code_data_key] = JSON.stringify(link_code_data);
  }

  generateLinkCode(wallet, authorization_data) {
    let link_code_data = {
      expires_at: authorization_data.claims.exp,
      token: `Bearer ${authorization_data.token}`,
      uri: `${this.configuration.uri.api}/wallet/${wallet.id}/link`,
      wallet_description: wallet.description
    };

    if(angular.isDefined(authorization_data.description)) {
      link_code_data.description = authorization_data.description;
    }

    let encrypted_link_code = window.CryptoJS.AES.encrypt(JSON.stringify(link_code_data), authorization_data.password).toString();
    let link_code_id = encrypted_link_code.replace(/[^a-z0-9]+/gi, '').substr(0, 25);

    localStorage.removeItem(this.link_code_data_key);

    return this.$http({
      data: {
        expires_in: Math.floor(authorization_data.claims.exp - (Date.now() / 1000)),
        id: link_code_id,
        identifier_link_code: encrypted_link_code
      },
      headers: this.Authorization.getHeaders(),
      method: 'POST',
      url: `${this.configuration.uri.api}/identifier_link_code`
    }).then(() => {
      this.$analytics.eventTrack('generate_link:success', { category: 'identifier' });

      // The following is stored both to show the link code and to be able to
      // reinstate the form after it times out
      link_code_data.description = authorization_data.description;
      link_code_data.expiration_s = authorization_data.expiration_s;
      link_code_data.id = link_code_id;
      link_code_data.password = authorization_data.password;
      link_code_data.url_qr = qr.svgObject(this.getUrlForLinkCode(link_code_data.id, authorization_data.password));
      link_code_data.url_qr.viewBox = `0 0 ${link_code_data.url_qr.size} ${link_code_data.url_qr.size}`;

      return link_code_data;
    });
  }

  getLinkCodeData(wallet = null) {
    try {
      let link_code_data = localStorage[this.link_code_data_key];

      if(angular.isDefined(link_code_data)) {
        link_code_data = JSON.parse(link_code_data);

        // The available link code data will be returned either if simply asked
        // for, but it can be filtered to a specific wallet
        if(!wallet || link_code_data.wallet === wallet.id) {
          return link_code_data;
        }
      }
    } catch(e) {
      this.ErrorService.add('',
        'Cannot parse stored link code data. Please try again.');
    }

    return false;
  }

  getUrlForLinkCode(id, password = '') {
    let payload = this.PayloadService.encrypt({ id, password });
    return `outlaymanwallet://#/link-identifier?payload=${payload}`;
  }

  link(wallet, credential) {
    this.$analytics.eventTrack('add:initialize', { category: 'identifier' });

    return this.$http({
      data: { credential },
      headers: this.Authorization.getHeaders(),
      method: 'POST',
      url: `${this.configuration.uri.api}/wallet/${wallet.id}/link`
    }).then(() => {
      this.$analytics.eventTrack('add:success', { category: 'identifier' });
      wallet.refresh();
    }, () => {
      this.$analytics.eventTrack('add:failure', { category: 'identifier' });
    });
  }

  _notifySubscribers() {
    this.wallets$.onNext(_.values(this.wallets));
  }

  requestLinkCode(wallet, link_code_data) {
    this.$analytics.eventTrack('generate_link:initialize', { category: 'identifier' });

    localStorage[this.link_code_data_key] = JSON.stringify({
      description: link_code_data.description,
      expiration_s: link_code_data.expiration_s,
      password: link_code_data.password,
      wallet: wallet.id
    });

    this.Authorization.authorize(this.$location.path(), [ 'wallet:link_own' ],
      link_code_data.expiration_s);
  }

  unlink(wallet, credential) {
    return this.$http({
      data: { credential },
      headers: this.Authorization.getHeaders(),
      method: 'POST',
      url: `${this.configuration.uri.api}/wallet/${wallet.id}/unlink`
    }).then(() => {
      this.$analytics.eventTrack('remove:success', { category: 'identifier' });
      wallet.refresh();
    }, () => {
      this.$analytics.eventTrack('remove:failure', { category: 'identifier' });
    });
  }
}
