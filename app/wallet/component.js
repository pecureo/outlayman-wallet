'use strict';

import moment from 'moment';

import './component.scss';

export default ng_app_module => {
  ng_app_module.component('wallet', {
    bindings: { walletId: '<' },
    controller: WalletController,
    template: require('./component.html')
  });
};

class WalletController {
  constructor($analytics, $element, $mdDialog, $mdToast, $scope, $timeout,
    CredentialService, ErrorService, NfcReader, WalletService) {

    this.$analytics = $analytics;
    this.$element = $element;
    this.$mdDialog = $mdDialog;
    this.$mdToast = $mdToast;
    this.$timeout = $timeout;
    this.CredentialService = CredentialService;
    this.WalletService = WalletService;

    this.adding_credential = false;
    this.expirations = [ 60, 120, 180, 300, 600 ];
    this.loading = false;
    this.settings_tab = 0;
    this.settings_visible = false;

    this.clearCredential();
    this.clearIdentifierLinkCodeData();

    this.transaction_date_from = moment().subtract(1, 'month').startOf('day').toDate();
    this.transaction_date_until = moment().toDate();

    WalletService.wallets$.subscribe(wallets => {
      this.show_back_button = (wallets.length > 1);
      wallets = wallets.filter(wallet => (this.walletId == wallet.id));

      if(wallets.length === 1) {
        this.wallet = wallets[0];

        let identifier_link_code_data = WalletService.getLinkCodeData(this.wallet);
        if(identifier_link_code_data) {
          WalletService.generateLinkCode(this.wallet, identifier_link_code_data).then(link_code_data => {
            this.settings_visible = true;
            this.show_add_identifier = true; // Make sure the 'Add Identifier' view is visible

            this.identifier_link_code.id = link_code_data.id;
            this.identifier_link_code.url_qr = link_code_data.url_qr;

            this.identifier_link_code.timeout = $timeout(() => {
              $analytics.eventTrack('generate_link:expired', { category: 'identifier' });
              ErrorService.add('', 'The identifier link code has expired, please generate a new one.');
              this.identifier_link_code.id = null;
              this.identifier_link_code.url_qr = null;

              // Restore the previous values for easy regeneration of the code
              this.identifier_link_code.description = link_code_data.description;
              this.identifier_link_code.expiration_s = link_code_data.expiration_s;
              this.identifier_link_code.password = link_code_data.password;
            }, (identifier_link_code_data.claims.exp * 1000) - Date.now());
          });
        }

        this.filterTransactions();
      }

      $timeout($scope.$apply.bind($scope));
    });

    NfcReader.is_available$.subscribe(is_available => {
      this.nfc_is_available = is_available;

      NfcReader.reader.subscribe(tag => {
        this.new_tag.id = tag;
        $scope.$apply();
      });
    });

  }

  addCredential(new_tag) {
    this.adding_credential = true;

    let credential_data = { uuid: new_tag.id };
    if(new_tag.description) {
      credential_data.description = new_tag.description;
    }

    this.WalletService.link(this.wallet, credential_data).then(() => {
      this.adding_credential = false;
      this.clearIdentifierLinkCodeData();
      this.clearCredential();
      this.refresh();

      this.$mdToast.show(this.$mdToast.simple({
        parent: this.$element,
        position: 'top right'
      }).textContent(
        'Added identifier'
      ));
    }, () => {
      this.adding_credential = false;

      this.$mdToast.show(this.$mdToast.simple({
        parent: this.$element,
        position: 'top right'
      }).textContent(
        'Failed to add identifier'
      ));
    });

  }

  canAddNewCredential() {
    return !this.adding_credential && this.new_tag.id && !this.isKnownCredential(this.new_tag);
  }

  canClearCredential() {
    return this.new_tag.id || this.new_tag.description;
  }

  canShareLinkCode() {
    return !!window.socialmessage;
  }

  clearCredential() {
    this.new_tag = { id: null, description: null };
  }

  clearIdentifierLinkCodeData() {
    if(this.identifier_link_code && this.identifier_link_code.timeout) {
      this.$timeout.cancel(this.identifier_link_code.timeout);
    }

    this.identifier_link_code = {
      description: null,
      expiration_s: this.expirations[1],
      id: null,
      password: null,
      retrieving: false
    };
  }

  exportTransactions(targetEvent) {
    // The values are tab-separated, as the description may include commans
    // messing with the format of a comma-separated markup
    let overview = this.transactions.reduce((overview, transaction) => {
      overview.push([
        moment(transaction.createdAt),
        transaction.amount.toFixed(2),
        transaction.description
      ].join('\t'));

      return overview;
    }, [ [ 'Date', 'Amount (€)', 'Description' ].join('\t') ]);

    let friendly_from = moment(this.transaction_date_from).format('LL');
    let friendly_until = moment(this.transaction_date_until).format('LL');
    let filename = `Transaction Export for ${this.wallet.description} from ${friendly_from} until ${friendly_until}.tsv`;

    let tsv_blob = new Blob(
      [ overview.join('\n') ],
      { type: 'text/tab-separated-values; charset=utf-8' }
    );

    if(navigator.msSaveBlob) {
      navigator.msSaveBlob(tsv_blob, filename);
    } else {
      // Downloading the TSV as a file does not work in all cases (for instance
      // when running inside Cordova). For these cases the TSV is displayed in
      // a textarea inside a dialog
      let link = document.createElement('a');
      if(!window.cordova && window.angular.isDefined(link.download)) {
        link.setAttribute('download', filename);
        link.setAttribute('href', URL.createObjectURL(tsv_blob));
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      } else {
        this.$mdDialog.show({
          clickOutsideToClose: true,
          controller: function($mdDialog) {
            this.close = () => $mdDialog.hide();
            this.export_height = Math.min(10, overview.length);
            this.title =  filename.substr(0, filename.length - 4);
          },
          controllerAs: '$ctrl',
          fullscreen: true,
          targetEvent,
          template: `
            <md-dialog aria-label="{{:: $ctrl.title }}">
              <md-toolbar class="md-toolbar-tools">
                <h3>{{:: $ctrl.title }}</h3>
              </md-toolbar>
              <md-dialog-content layout-padding>
                <md-input-container class="md-block">
                  <textarea aria-label="Exported Entries" md-select-on-focus
                            rows="{{:: $ctrl.export_height}}">${overview.join('\n')}</textarea>
                </md-input-container>
              </md-dialog-content>
              <md-dialog-actions>
                <md-button class="md-primary" ng-click="$ctrl.close()">
                  Close
                </md-button>
              </md-dialog-actions>
            </md-dialog>
          `
        });
      }
    }
  }

  filterTransactions() {
    if(this.wallet && this.transaction_date_from && this.transaction_date_until) {
      this.transactions = this.wallet.transactions.filter(transaction => {
        return moment(transaction.createdAt)
          .isBetween(this.transaction_date_from, this.transaction_date_until);
      });
    }
  }

  isKnownCredential(credential) {
    return this.wallet.credentials.some(wallet_credential => {
      return wallet_credential.uuid === credential.id;
    });
  }

  isMainAccountCredential(credential) {
    return (credential.uuid === this.wallet.owner.uuid);
  }

  refresh() {
    this.$analytics.eventTrack('refresh', { category: 'wallet' });
    this.loading = true;

    return this.wallet.refresh().then(() => {
      this.loading = false;
      this.filterTransactions();
    });
  }

  removeCredential(credential) {
    // Do not allow the main account UUID to be removed from a wallet
    if(credential.uuid !== this.wallet.owner.uuid) {
      this.$analytics.eventTrack('remove:initialize', { category: 'identifier' });
      this.WalletService.unlink(this.wallet, credential);
    }
  }

  requestIdentifierLinkCode(identifier_link_code_data) {
    // No need to set back to false as the process will cause a reload of the
    // application, which will reset these values back to their defaults
    this.identifier_link_code.retrieving = true;
    this.loading = true;
    this.WalletService.requestLinkCode(this.wallet, identifier_link_code_data);
  }

  saveCredentialDescription(credential, value) {
    this.CredentialService.update(credential.uuid, 'description', value);
  }

  shareLinkCode() {
    this.$analytics.eventTrack('generate_link:share', { category: 'identifier' });

    window.socialmessage.send({
      url: this.WalletService.getUrlForLinkCode(this.identifier_link_code.id)
    });
  }

  toggleAddIdentifier() {
    this.clearIdentifierLinkCodeData();
    this.show_add_identifier = !this.show_add_identifier;
  }

  toggleSettings() {
    this.settings_visible = !this.settings_visible;
  }
}
