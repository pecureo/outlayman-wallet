'use strict';

import 'angular';
import 'angular-animate';
import 'angular-aria';
import 'angular-material';
import 'angular-material/angular-material.css';
import 'angular-messages';
import 'angular-restmod/dist/angular-restmod-bundle';
import 'angular-restmod/dist/styles/ams';
import 'angular-route';
import 'angulartics';
import 'angulartics-google-analytics';
import Rx from 'rx';

// This module has to be loaded first, as it will make CryptoJS available on
// the global window which will be extended below
import AuthorizationModule from './authorization';

// The order of these imports is important, as they extend each other!
require('crypto-js/enc-base64');
require('crypto-js/md5');
require('crypto-js/evpkdf');
require('crypto-js/cipher-core');
require('crypto-js/aes');

import ActionMenu from './components/action-menu';
import CreateTransaction from './components/create-transaction';
import Credential from './credential';
import EditableListItem from './editable-list-item';
import ErrorService from './error.service';
import Inbox from './components/inbox';
import LinkIdentifier from './components/link-identifier';
import Nfc from './nfc';
import PayloadService from './payload.service';
import Profile from './components/profile';
import Registration from './components/registration';
import Wallet from './wallet';

import './index.scss';

// Register a (bare) service worker for 'Add to homescreen' to work
if('serviceWorker' in navigator) {
  navigator.serviceWorker.register('./service-worker.js');
}

let outlayman_native_uri$ = new Rx.Subject();
let ng_outlayman = angular.module('outlayman', [ 'angulartics',
  'angulartics.google.analytics', 'anvil', 'ngAnimate', 'ngMaterial',
  'ngMessages', 'ngRoute', 'restmod' ]);

ActionMenu(ng_outlayman);
AuthorizationModule(ng_outlayman);
CreateTransaction(ng_outlayman);
Credential(ng_outlayman);
EditableListItem(ng_outlayman);
ErrorService(ng_outlayman);
Inbox(ng_outlayman);
LinkIdentifier(ng_outlayman);
Nfc(ng_outlayman);
PayloadService(ng_outlayman);
Profile(ng_outlayman);
Registration(ng_outlayman);
Wallet(ng_outlayman);

function processOutlaymanNativeUri(uri) {
  let android_uri = uri.replace(/outlaymanwallet:\/\/(#\/)?/,
                                'file:///android_asset/www/index.html#/');
  outlayman_native_uri$.onNext(android_uri);
}
window.handleOpenURL = processOutlaymanNativeUri;

angular.element(document).ready(() => {
  let bootstrap_outlayman = () => angular.bootstrap(document.body, ['outlayman']);

  // When running from a file location the likely scenario is being run inside
  // a Cordova environment, so an additional script should be loaded to enable
  // hardware support for NFC and such
  if(/file\:/.test(window.location.protocol)) {
    let cordova_script = document.createElement('script');
    Object.assign(cordova_script, { charset: 'utf-8', src: 'cordova.js' });
    document.head.appendChild(cordova_script);

    document.addEventListener('deviceready', bootstrap_outlayman, false);
  } else {
    bootstrap_outlayman();
  }
});

ng_outlayman.factory('SailsApi', function(Authorization, inflector, restmod) {
  return restmod.mixin({
    $config: {
      style: 'Sails',
      primaryKey: 'id',
      jsonMeta: 'meta',
      jsonLinks: 'links'
    },
    $extend: {
      // snake_case to CamelCase conversion
      Model: {
        decodeName: inflector.camelize,
        encodeName: name => inflector.parameterize(name, '_'),
        encodeUrlName: inflector.parameterize
      }
    },
    $hooks: {
      'before-request': function(req) {
        req.headers = Authorization.getHeaders(req.headers);
      }
    }
  });
});

ng_outlayman.factory('$exceptionHandler', function($log) {
  return (exception, cause) => {
    $log.error(exception, cause);
  };
});

ng_outlayman.config(function($compileProvider, $locationProvider,
  $routeProvider, restmodProvider) {

  let payload_param_parser = category => {
    return [ '$analytics', '$location', '$route', ($analytics, $location, $route) => {
      let payload_storage_key = 'outlayman.payload';
      let stored_payload = localStorage.getItem(payload_storage_key);

      if(stored_payload) {
        $analytics.eventTrack('open_from_link', { category });
        localStorage.removeItem(payload_storage_key);
        return stored_payload;
      } else if($route.current.params.payload) {
        localStorage.setItem(payload_storage_key, $route.current.params.payload);

        // Clear search parameters to prevent the payload from being picked up
        // multiple times through navigating around the application
        $location.search({}).replace();
      }

      return null;
    } ];
  };

  $compileProvider.debugInfoEnabled(false);

  $locationProvider.html5Mode(false);

  $routeProvider.when('/create-transaction', {
    resolve: { payload: payload_param_parser('transaction') },
    template: '<create-transaction payload="$resolve.payload"></create-transaction>'
  });
  $routeProvider.when('/inbox', { template: '<inbox></inbox>' });
  $routeProvider.when('/link-identifier', {
    resolve: { payload: payload_param_parser('credential') },
    template: '<link-identifier payload="$resolve.payload"></link-identifier>'
  });
  $routeProvider.when('/profile', { template: '<profile></profile>' });
  $routeProvider.when('/register', { template: '<registration></registration>' });
  $routeProvider.when('/wallet/:id', {
    resolve: { wallet_id: $route => $route.current.params.id },
    template: '<wallet layout="column" wallet_id="$resolve.wallet_id"></wallet>'
  });
  $routeProvider.otherwise({ redirectTo: '/authorize' });

  restmodProvider.rebase('SailsApi');
});

ng_outlayman.constant('configuration', require('configuration'));

ng_outlayman.run(($analytics, $http, $location, $rootScope, $route, $window,
  AnnouncementService, Authorization, ErrorService, WalletService) => {

  Authorization.session$.subscribe(session => {
    if(session.id_token) {
      $analytics.eventTrack('sign_in:success', { category: 'credential' });
    }
  });

  outlayman_native_uri$.subscribe(uri => {
    $window.location.href = uri;
  });

  $rootScope.$on('$locationChangeStart', (event, uri) => {
    let authorization_data_index = uri.indexOf('access_token');

    if(authorization_data_index >= 0) {
      let authorization_data = uri.slice(authorization_data_index);
      let identifier_link_code_data = WalletService.getLinkCodeData();
      $location.hash('').replace();

      authorization_data = Authorization.process(authorization_data);
      if(authorization_data.error) {
        ErrorService.add('', authorization_data.error);
        if(identifier_link_code_data) {
          $analytics.eventTrack('generate_link:failure', { category: 'identifier' });
        } else {
          Authorization.sign_out();
          $analytics.eventTrack('sign_in:failure', { category: 'credential' });
        }
      } else if(identifier_link_code_data) {
        $analytics.eventTrack('generate_link:generated', { category: 'identifier' });
        WalletService.addCredentialsToLinkCode(authorization_data);
      } else {
        Authorization.updateSession(authorization_data);
      }

      let authorized_destination = Authorization.destination(false);
      if(authorized_destination) {
        $location.url(authorized_destination);
      }
    }
  });
});

class OutlaymanController {
  constructor($location, $mdDialog, ActionMenuService, AnnouncementService,
    Authorization, ErrorService, NfcReader, configuration) {

    this.AnnouncementService = AnnouncementService;

    this.version = configuration.version;
    this.signed_in = false;

    let action_menu$ = ActionMenuService.getMenu$('outlayman-wallet');

    action_menu$.subscribe(action_menu => {
      this.sidenav = action_menu;

      let create_transaction_action = {
        analytics: {
          category: 'transaction',
          event: 'navigate_through_menu'
        },
        description: 'Create Transaction',
        icon: 'compare_arrows',
        payload: '/create-transaction'
      };

      let inbox_link_action = {
        analytics: {
          category: 'inbox',
          event: 'navigate_through_menu'
        },
        description: 'Inbox',
        icon: 'email',
        payload: '/inbox',
        weight: -1
      };

      let profile_link_action = {
        analytics: {
          category: 'profile',
          event: 'navigate_through_menu'
        },
        description: 'Profile',
        icon: 'account_box',
        payload: '/profile',
        weight: -2
      };

      let show_about_action = {
        analytics: {
          category: 'about',
          event: 'show_from_menu'
        },
        description: 'About',
        icon: 'info',
        payload: () => {
          let title = `About Outlayman Wallet v${this.version}`;

          $mdDialog.show(
            $mdDialog.alert()
              .ariaLabel(title)
              .clickOutsideToClose(true)
              .ok('Close')
              .targetEvent(event)
              .textContent('The wallet management application for the Outlayman Payment System')
              .title(title)
          );
        }
      };

      action_menu.register(profile_link_action);
      action_menu.register(inbox_link_action);
      action_menu.register(create_transaction_action);
      action_menu.register(show_about_action, 'secondary');

      if(window.cordova && window.cordova.plugins && window.cordova.plugins.barcodeScanner) {
        let scan_qr_code_action = {
          analytics: {
            category: 'qr_scan',
            event: 'navigate_through_menu'
          },
          description: 'Scan QR Code',
          icon: 'photo_camera',
          payload: () => {
            window['cordova'].plugins.barcodeScanner.scan(result => {
              processOutlaymanNativeUri(result.text);
            }, () => {
              ErrorService.add('Failed to scan QR code', '');
            }, {
              formats: 'QR_CODE'
            });
          }
        };

        action_menu.register(scan_qr_code_action);
      }

      if(!Authorization.implicit) {
        let signout_action = {
          analytics: {
            category: 'credential',
            event: 'sign_out'
          },
          description: 'Sign Out',
          icon: 'exit_to_app',
          payload: () => Authorization.signout('/authorize?signed_out=true')
        };

        action_menu.register(signout_action, 'secondary');
      }
    });

    // Define this action outside of the subscription so it can be removed and
    // re-added when availability of the NFC reader changes
    let identifier_link_action = {
      analytics: {
        category: 'identifier',
        event: 'add_through_link:from_menu'
      },
      description: 'Link Identifier to Wallet',
      icon: 'redeem',
      payload: '/link-identifier'
    };

    action_menu$
      .combineLatest(NfcReader.is_available$)
      .subscribe(([action_menu, is_available]) => {
        if(is_available) {
          action_menu.register(identifier_link_action);
        } else {
          action_menu.unregister(identifier_link_action);
        }
      });

    AnnouncementService.announcement$.subscribe(announcements => {
      this.announcement = announcements[announcements.length - 1];
    });

    Authorization.scopes$.subscribe(scopes => {
      this.signed_in = scopes.includes('credential:view_own');

      // In case the set of available scopes matches this set exactly, the
      // registration procedure has not been completed yet
      let unregistered_scopes = [ 'openid', 'profile', 'email' ];

      if(scopes.length === 0) {
        // If no scopes are available, the authorization procedure has not been
        // completed yet
        let after_authorization_target = $location.url();
        if(after_authorization_target.length === 0) {
          after_authorization_target = '/profile';
        } else {
          let search_parameter_name = 'after_authorization_target';
          let search_parameter_index = after_authorization_target.indexOf(search_parameter_name) + 1;
          if(search_parameter_index > 0) {
            after_authorization_target = decodeURIComponent(
              after_authorization_target.substr(search_parameter_index + search_parameter_name.length)
            );
          }
        }

        // If the current URI references the callback route, the authorization
        // flow is in progress and should not be interrupted
        if(after_authorization_target.indexOf('/callback') === -1) {
          if(Authorization.implicit) {
            Authorization.authorize(after_authorization_target);
          } else {
            $location.path('/authorize').search({ after_authorization_target });
          }
        }
      } else if(scopes.length === unregistered_scopes.length &&
                scopes.includes(...unregistered_scopes)) {
        $location.path('/register');
      }
    });
  }

  markAsRead(announcement) {
    this.AnnouncementService.markAsRead(announcement);
    this.AnnouncementService.refresh();
  }

  toggleActionMenu(event) {
    // The toggle function could theoretically be triggered before a user has
    // signed into the application, so this additional check is necessary
    if(this.signed_in && this.sidenav) {
      switch(event.type) {
      case 'click':
        this.sidenav.toggle();
        break;
      case '$md.swipeleft':
        if(this.sidenav.isOpen()) {
          this.sidenav.close();
        }
        break;
      case '$md.swiperight':
        // Swiping right should only open the action menu if the swipe
        // originates from the left 10% of the screen
        if(!this.sidenav.isOpen()) {
          // The HTML tag is the second to last element in the path
          let html_width = event.path[event.path.length - 3].clientWidth;
          if(event.pointer.startX / html_width < 0.1) {
            this.sidenav.open();
          }
        }
        break;
      }
    }
  }
}

ng_outlayman.controller('OutlaymanController', OutlaymanController);
