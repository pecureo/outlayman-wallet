'use strict';

let webpack = require('webpack');
let FaviconsWebpackPlugin = require('favicons-webpack-plugin');
let HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  context: __dirname + '/app',
  entry: {
    bundle: './index.js',
    'service-worker': './service-worker.js'
  },
  module: {
    loaders: [
      { test: /\.css$/, loaders: ['style', 'css'] },
      { test: /\.(eot|woff2?|ttf|svg)$/, loader: 'file' },
      { test: /\.html$/, loader: 'raw' },
      { test: /\.js$/, loaders: ['ng-annotate', 'babel?presets[]=es2015'], exclude: /node_modules/ },
      { test: /\.scss$/, loaders: ['style', 'css', 'sass'] }
    ]
  },
  output: {
    path: __dirname + '/dist',
    filename: '[name].js'
  },
  plugins: [
    new webpack.optimize.DedupePlugin(),
    new FaviconsWebpackPlugin({
      background: '#ffffff',
      icons: {
        android: true,
        appleIcon: true,
        appleStartup: true,
        favicons: true,
        firefox: true
      },
      logo: './icon.png',
      prefix: './',
      theme_color: '#3f51b5',
      title: 'Outlayman Wallet'
    }),
    new HtmlWebpackPlugin({
      inject: false,
      template: 'index.ejs'
    }),
    new webpack.optimize.UglifyJsPlugin()
  ],
  resolve: {
    alias: {
      'anvil-connect': __dirname + '/bower_components/anvil-connect',
      'configuration': __dirname + '/app/config/production.web.js',
      'crypto-js': __dirname + '/bower_components/crypto-js/components',
      'jsrsasign': __dirname + '/bower_components/jsrsasign',
      'jsjws': __dirname + '/bower_components/jsjws',
      'nfc-reader': __dirname + '/app/nfc/web.js',
      'sjcl': __dirname + '/bower_components/sjcl'
    }
  }
};
