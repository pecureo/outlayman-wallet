'use strict';

let config = require('./webpack.config.js');
let environment = process.env.NODE_ENV || 'development';

if(environment === 'development') {
  config.devtool = 'eval-source-map';
}

config.output.path = __dirname + '/cordova/www';
config.resolve.alias['configuration'] = __dirname + '/app/config/' + environment + '.cordova.js';
config.resolve.alias['nfc-reader'] = __dirname + '/app/nfc/cordova.js';

module.exports = config;
